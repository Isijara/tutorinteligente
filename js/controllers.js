function loadingController ($scope) {}

var callModal = function() {
    $('.modal').modal({keyboard: true});
};

function homeController($scope, $http) {

      !(function() {
            // Grab elements, create settings, etc.
            var canvas = document.getElementById("canvas"),
                context = canvas.getContext("2d"),
                video = document.getElementById("video"),
                videoObj = { "video": true },
                errBack = function(error) {
                    console.log("Video capture error: ", error);
                };

            // Put video listeners into place
            if(navigator.getUserMedia) { // Standard
                navigator.getUserMedia(videoObj, function(stream) {
                    alert("en videoObj");
                    video.src = stream;
                    video.play();
                }, errBack);
            } else if(navigator.webkitGetUserMedia) { // WebKit-prefixed
                navigator.webkitGetUserMedia(videoObj, function(stream){
                    video.src = window.webkitURL.createObjectURL(stream);
                    video.play();
                }, errBack);
            }
            else if(navigator.mozGetUserMedia) { // Firefox-prefixed
                navigator.mozGetUserMedia(videoObj, function(stream){
                    video.src = window.URL.createObjectURL(stream);
                    video.play();
                }, errBack);
            }

            document.getElementById("snap").addEventListener("click", function() {
                context.drawImage(video, 0, 0, 640, 480);
            });
        })();

}

function navigationController($scope) {
    $scope.class = {};
    window.navigationScope = $scope;
    var url = document.URL.split(/\//);
    $scope.url = url[url.length-1];
    $scope.urls = [];

    $scope[$scope.url] = 'active';
    $scope.updateSelection = function(e) {

        $scope.url = document.URL.split(/\//);
        $scope.url = url[url.length-1];

        if( _.indexOf($scope.urls, $scope.url) < 0) {
            $scope.urls.push($scope.url);
        }

        $("ul.navigationMenu li").each(function(){
            $(this).removeClass("active");
        });
        var element = angular.element(e.srcElement);

        $(element).parent().addClass("active");
    };

    $scope.callbackNavigation = function ($scope) {
        $scope.url = document.URL.split(/\//);
        $scope.url = $scope.url[$scope.url.length-1];

        if( _.indexOf($scope.urls, $scope.url) < 0) {
            $scope.urls.push($scope.url);
        }

        console.log($scope.urls);
        _.each($scope.urls, function(element) {
            if( element != $scope.url ) {
                $scope[$scope.url] = '';
            }
            console.log('element', element, typeof(element));
        });
        $scope[$scope.url] = 'active';
    };


    $scope.updateSelection("");
}

function tutorController ($scope) {

    window.scope = $scope;

    $scope.tablas = [];
    $scope.generarPreguntas = function () {
        while( $scope.tablas.length !== 10 ) {
            var n1 = Math.floor(Math.random() * 10) + 1,
                n2 = Math.floor(Math.random() * 10) + 1,
                labelTabla = n1 + ' x ' + n2,
                resultado  = n1 * n2,
                element    = { labelTabla: labelTabla, resultado: resultado };

            if( !_.contains($scope.tablas, element) ) {
                $scope.tablas.push(element);
            }
        }
    };

    $scope.tablasAyuda = function() {
        Peedy.speak('Multiplica los números y anota el resultado!');
    };

    $scope.generarPreguntas();

    var ejercicioTablas = function () {
        var ejercicio = function() {
            var self = this;
            self.niveles = {
                bajo : {
                    operaciones : [],
                    aciertos : 0
                } ,
                medio: {
                    operaciones : [],
                    aciertos : 0
                },
                alto: {
                    operaciones : [],
                    aciertos: 0
                }
            };
            ejercicio.prototype.esNivelAlto = function() {
                var self = this;
                return (self.niveles.alto.aciertos < 2 && self.niveles.medio.aciertos >=2 );
            };

            ejercicio.prototype.esNivelMedio = function() {
                var self = this;
                return (self.niveles.medio.aciertos < 2 && self.niveles.bajo.aciertos >=2 );
            };

            ejercicio.prototype.esNivelBajo = function() {
                var self = this;
                return (self.niveles.alto.aciertos < 2 );
            };

            ejercicio.prototype.getNivelEstudiante = function() {

                var self = this,
                    nivelAlto = 10, nivelMedio = 6, nivelBajo = 3, resultado;

                if( self.esNivelAlto() )  {
                    console.log('Ejecutar acción nivel alto');
                } else if( self.esNivelMedio() ) {
                    console.log('Ejecutar acción nivel medio');
                } else if( self.esNivelBajo() ) {
                    console.log('Ejecutar acción nivel bajo');
                }
            };
        };

        return new ejercicio();
    };
}






