var master = angular.module('master', ['ngRoute']);


master.directive('navigation', function() {
    return {
        templateUrl: 'partials/navigation.html',
        replace: true,
        restrict: 'E'
    };
});

master.config([ '$routeProvider', function($routeProvider){
    $routeProvider.when('/home', {
        templateUrl: 'partials/home.html',
        controller: homeController
    })
    .when('/about', {
        templateUrl: 'partials/about.html'
    }).
    when( '/tutor', {
        templateUrl: 'partials/tutor.html',
        controller: tutorController
    }).
    when('/juan', {
        templateUrl: 'partials/juanTest.html',
        controller: juanController
    })
    .otherwise({
        redirectTo: '/home'
    });
}]);

