<?php
$classesDir = array (
    '../models/'
);
require_once('../models/Biblioteca.php');

function __autoload($class_name) {
    global $classesDir;
    foreach ($classesDir as $directory) {
        if (file_exists($directory . $class_name . '.php')) {
            require_once ($directory . $class_name . '.php');
            return;
        }
    }
}/*
require_once('../models/DataBase.php');
require_once('../models/Configuracion.php');
require_once('../models/PrestamoLibros.php');
require_once('../models/Libro.php');
require_once('../models/Usuario.php');
require_once('../models/PrestamoNotificaSurtidoresDecorador.php');
require_once('../models/SurtidorFactory.php');
require_once('../models/SurtidorLibrosFactory.php');
require_once('../models/ObtencionLibrosProfesor.php');
require_once('../models/ObtencionLibrosEstudiante.php');
require_once('../models/ObtencionLibrosPublicoGeneral.php');
require_once('../models/Biblioteca.php');*/
$data = file_get_contents('php://input');
$peticionJson = json_decode($data);

$dataBase = dataBase::getInstance();
$dataBase->connectToLibraryDataBase();


    switch( $peticionJson->opcion)
    {
        case 'getCatalogo':
            echo json_encode(getCatalogo());
            break;

        case 'getConfiguracionSistema':
            echo json_encode(getConfiguracionSistema());
            break;
        case 'guardarConfiguracionSistema':
            echo json_encode(guardarConfiguracionSistema($peticionJson->guardarSurtidosEnMemoria, $peticionJson->guardarSurtidosEnArchivo));
            break;

        case 'getUsuariosConPrestamo':
            echo json_encode(getUsuariosConPrestamo());
            break;
        case 'getLibrosPrestadosPorClaveUsuario':
            echo json_encode(getLibrosPrestadosPorClaveUsuario($peticionJson->usuarioClave));
            break;
        case 'getUsuarios':
            echo json_encode(getUsuarios());
            break;
        case 'prestarLibros':
            echo json_encode(prestarLibros($peticionJson->claveUsuario, $peticionJson->isbns));
            break;
        case 'devolverLibros':
            echo json_encode(devolverLibros($peticionJson->usuarioClave, $peticionJson->libros));
            break;
            #Los libros de devolverLibros deberán de traer consigo el estado para poder incrementar adecuadamente la existencia. Deberá ser una arreglo de objetos. Necesita modificarse el front
        case 'getLibrosParaResurtir':
            echo json_encode(getLibrosParaResurtir());
            break;
    }
    function getLibrosParaResurtir() {
        # code...
        $biblioteca = Biblioteca::getInstance("","");
        $libros = $biblioteca->getLibrosParaSurtir();
        $resultado = array();
        foreach ($libros as $key => $libro) {
            $row['isbn'] = $libro->getISBN();
            $row['titulo'] = $libro->getTitulo();
            $row['editorial'] = $libro->getEditorial();
            $row['estado'] = $libro->getEstado();
            $row['autor'] = $libro->getAutor();
            $resultado[] = $row;
        }

        return $resultado;
    }
    function devolverLibros($usuarioClave, $librosDelRequest) {
        $prestamo = new PrestamoLibros();
        $prestamo = new PrestamoNotificaSurtidoresDecorador($prestamo);
        $prestamo->addSurtidor(SurtidorLibrosFactory::getFactory()->createSurtidor());
        $obtencionLibros = new ObtencionLibrosEstudiante();
        $biblioteca = Biblioteca::getInstance($prestamo, $obtencionLibros);
        $libros = array();
        foreach ($librosDelRequest as $key => $l) {
            $libro = new Libro();
            $libro->find($l->isbn, $l->estado);
            $libros[] = $libro;
        }
        $biblioteca->devolverLibros($usuarioClave, $libros);
    }

    function prestarLibros($usuarioClave, $isbns) { #clave y array de isbns

        $usuario = new Usuario();
        $usuario->find($usuarioClave);
        $prestamo = new PrestamoLibros();
        $prestamo = new PrestamoNotificaSurtidoresDecorador($prestamo);
        $prestamo->addSurtidor(SurtidorLibrosFactory::getFactory()->createSurtidor());
        $obtencionLibros;
        switch( $usuario->getTipo() ) {
            case Usuario::TIPO_ALUMNO:
                $obtencionLibros = new ObtencionLibrosEstudiante();
                break;
            case Usuario::TIPO_PROFESOR :
                $obtencionLibros = new ObtencionLibrosProfesor();
                break;
            case Usuario::TIPO_PUBLICO :
                $obtencionLibros = new ObtencionLibrosPublicoGeneral();
                break;
        }
        $biblioteca = Biblioteca::getInstance($prestamo, $obtencionLibros);
        $biblioteca->prestarLibro($usuario, $isbns);

    }

    function getUsuarios() {
        $objUsuario = new Usuario();
        $usuarios = $objUsuario->getUsuarios();
        $resultado = array();
        foreach ($usuarios as $key => $u) {
            $usuario['nombre'] = $u->getNombre();
            $usuario['apellidoPaterno'] = $u->getApellidoPaterno();
            $usuario['apellidoMaterno'] = $u->getApellidoMaterno();
            $usuario['tipo'] = $u->getTipo();
            $usuario['clave'] = $u->getClave();
            $resultado[] = $usuario;
        }
        return $resultado;
    }

    function getLibrosPrestadosPorClaveUsuario($usuarioClave) {
        $prestamoLibro = new PrestamoLibros();
        $libros = $prestamoLibro->getLibrosPrestadosPorClaveUsuario($usuarioClave);

        $resultado = array();
        foreach ($libros as $key => $libro) {
            $book['isbn'] = $libro->getISBN();
            $book['titulo'] = $libro->getTitulo();
            $book['editorial'] = $libro->getEditorial();
            $book['estado'] = $libro->getEstado();
            $book['autor'] = $libro->getAutor();
            $resultado[] = $book;
        }
        return $resultado;
    }

    function getConfiguracionSistema()
    {
        $config = Configuracion::getInstance();
        return array('guardarSurtidosEnMemoria' => $config->getGuardarSurtidosEnMemoria(),
                     'guardarSurtidosEnArchivo' => $config->getGuardarSurtidosEnArchivo());
    }

    function guardarConfiguracionSistema($guardarSurtidosEnMemoria, $guardarSurtidosEnArchivo) {
        $config = Configuracion::getInstance();
        $config->setGuardarSurtidosEnMemoria($guardarSurtidosEnMemoria);
        $config->setGuardarSurtidosEnArchivo($guardarSurtidosEnArchivo);
        $config->save();
    }

    function getUsuariosConPrestamo() {
        $prestamoLibros = new PrestamoLibros();
        $usuarios = $prestamoLibros->getUsuariosConPrestamo();
        $resultado = array();

        foreach ($usuarios as $key => $u) {
            $usuario['nombre'] = $u->getNombre();
            $usuario['apellidoPaterno'] = $u->getApellidoPaterno();
            $usuario['apellidoMaterno'] = $u->getApellidoMaterno();
            $usuario['tipo'] = $u->getTipo();
            $usuario['clave'] = $u->getClave();
            $resultado[] = $usuario;
        }

        return $resultado;
    }

    function getCatalogo() {
        $dataBase = dataBase::getInstance();
        $result =  mysql_query('SELECT isbn, titulo, autor, editorial, SUM( existencia ) AS existencia
                                    FROM  Libro GROUP BY ISBN') or die(mysql_error());

        $catalogo = array();
        if( mysql_num_rows($result) > 0 ) {
            while( $row = mysql_fetch_assoc($result) ) {
                $catalogo[] = $row;
            }
        }
        return $catalogo;
    }

?>